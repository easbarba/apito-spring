/*
 * ruokin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ruokin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ruokin. If not, see <https://www.gnu.org/licenses/>.
 */

package dev.easbarba.ruokin;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.Banner;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class RuokinApplication {
  static Logger log = LoggerFactory.getLogger(RuokinApplication.class);

  public static void main(String[] args) {
    new SpringApplicationBuilder()
        .sources(RuokinApplication.class)
        .logStartupInfo(false)
        .bannerMode(Banner.Mode.OFF)
        .lazyInitialization(true)
        .web(WebApplicationType.SERVLET)
        .run(args);
  }
}
