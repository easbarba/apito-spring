/*
 * ruokin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ruokin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ruokin. If not, see <https://www.gnu.org/licenses/>.
 */

package dev.easbarba.ruokin;

import dev.easbarba.ruokin.common.ResponseDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

// @CrossOrigin
// (
//     origins = {"http://localhost:8080"},
//     maxAge = 3600)
@RestController
@RequestMapping(path = "", produces = "application/json")
public class HomeController {
  public final Logger logger = LoggerFactory.getLogger(HomeController.class);

  @GetMapping("")
  // @CrossOrigin
  public ResponseEntity<ResponseDTO<String>> welcomeMessage() {
    var response = new ResponseDTO<String>("Welcome to Ruokin!", "success");

    return ResponseEntity.ok().allow(HttpMethod.GET).body(response);
  }
}
