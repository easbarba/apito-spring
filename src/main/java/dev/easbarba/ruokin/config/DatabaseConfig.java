/*
* ruokin is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* ruokin is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with ruokin. If not, see <https://www.gnu.org/licenses/>.
*/

package dev.easbarba.ruokin.config;

import org.flywaydb.core.Flyway;

import org.springframework.boot.autoconfigure.flyway.FlywayProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties({ FlywayProperties.class })
class DatabaseConfig {
    @Bean(initMethod = "migrate")
    public Flyway flyway(FlywayProperties flywayProperties) {
      return Flyway.configure()
        .dataSource(
            flywayProperties.getUrl(),
            flywayProperties.getUser(),
            flywayProperties.getPassword()
        )
        .locations(flywayProperties.getLocations()
          .stream()
          .toArray(String[]::new))
        .baselineOnMigrate(true)
        .load();
    }
}
