// /*
//  * ruokin is free software: you can redistribute it and/or modify
//  * it under the terms of the GNU General Public License as published by
//  * the Free Software Foundation, either version 3 of the License, or
//  * (at your option) any later version.
//  *
//  * ruokin is distributed in the hope that it will be useful,
//  * but WITHOUT ANY WARRANTY; without even the implied warranty of
//  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  * GNU General Public License for more details.
//  *
//  * You should have received a copy of the GNU General Public License
//  * along with ruokin. If not, see <https://www.gnu.org/licenses/>.
//  */

// package dev.easbarba.ruokin.config;

// import org.springframework.context.annotation.Configuration;
// import org.springframework.web.servlet.config.annotation.CorsRegistry;
// import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

// @Configuration
// public class WebConfig implements WebMvcConfigurer {

//   @Override
//   public void addCorsMappings(CorsRegistry registry) {
//     registry
//         .addMapping("/**")
//         .allowedOrigins("*")
//         .allowCredentials(true)
//         .allowedMethods("GET", "POST", "PUT", "DELETE", "OPTIONS")
//         .allowedHeaders("*")
//         .maxAge(3600);
//   }

//   // @Override
//   // public void addCorsMappings(CorsRegistry registry) {
//   //   registry
//   //       .addMapping("/**")
//   //       .allowedOrigins("http://localhost:8080")
//   //       .allowedMethods("GET", "POST", "PUT", "DELETE", "OPTIONS")
//   //       .allowedHeaders("*")
//   //       .allowCredentials(true);
//   // }
// }
