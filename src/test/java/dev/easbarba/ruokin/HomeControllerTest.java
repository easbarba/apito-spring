// ruokin is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// ruokin is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ruokin. If not, see <https://www.gnu.org/licenses/>.

package dev.easbarba.ruokin;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

@AutoConfigureMockMvc()
@SpringBootTest
@ActiveProfiles("test")
public class HomeControllerTest {

  @Autowired private MockMvc mockMvc;

  @Test
  @WithMockUser(value = "mussum")
  public void ShouldReturnHomeInfo() throws Exception {
     this.mockMvc
      .perform(get("").accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON))
      .andExpect(header().exists("Allow"))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.data", is("Welcome to Ruokin!")))
      .andReturn();
  }

  @Test
  @WithMockUser(value = "zacarias")
  public void ShouldReturnHealthInfo() throws Exception {
     this.mockMvc
      .perform(get("/health").accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON))
      .andExpect(header().exists("Allow"))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.data", is("UP")))
      .andReturn();
  }
}
