# ruokin is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ruokin is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ruokin. If not, see <https://www.gnu.org/licenses/>.

# DEPENDENCIES: podman, gawk, fzf, guix.

-include envs/.env.* # ENV FILES

.DEFAULT_GOAL := test
RUNNER ?= podman
POD_NAME := ruokin
NAME := ${POD_NAME}-spring
VERSION := $(shell gawk '/<version>/ { version=substr($$1,10,5); print version; exit }' pom.xml)
BACKEND_IMAGE := ${GITLAB_REGISTRY}/${USER}/${NAME}:${VERSION}
BACKEND_FOLDER := /app

# ================================= PORCELAIN

.PHONY: up
up: image.initial image.database image.start #image.security

.PHONY: down
down:
	${RUNNER} pod rm --force ${POD_NAME}
	${RUNNER} container rm --force ${DATABASE_NAME}
	${RUNNER} volume rm --force ${DATABASE_DATA}
	${RUNNER} container rm --force ${POD_NAME}

# ================================= IMAGES

.PHONY: image.initial
image.initial:
	${RUNNER} pod create \
		--publish ${BACKEND_PORT}:${BACKEND_INTERNAL_PORT} \
		--publish ${SPRING_LIVERELOAD_PORT}:${SPRING_LIVERELOAD_PORT} \
		--publish ${SECURITY_PORT}:${SECURITY_INTERNAL_PORT} \
		--publish ${FRONTEND_PORT}:${FRONTEND_INTERNAL_PORT} \
		--name ${POD_NAME}

.PHONY: image.database
image.database:
	${RUNNER} rm -f ${DATABASE_NAME}
	${RUNNER} run ${RUNNER_STATS} \
		--detach \
		--pod ${POD_NAME} \
		--name ${DATABASE_NAME} \
		--env POSTGRES_PASSWORD=${SQL_PASSWORD} \
		--env POSTGRES_USER=${SQL_USERNAME} \
		--env POSTGRES_DB=${SQL_DATABASE} \
		--volume ${DATABASE_DATA}:${SQL_DATA}:Z \
		${DATABASE_IMAGE}

.PHONY: image.database.repl
image.database.repl:
	${RUNNER} exec -it ${DATABASE_NAME} \
		psql --username ${SQL_USERNAME} --dbname ${SQL_DATABASE}

.PHONY: image.database.test
image.database.test:
	${RUNNER} rm -f ${DATABASE_NAME}-test
	${RUNNER} volume rm -f ${DATABASE_DATA}-test

	${RUNNER} run ${RUNNER_STATS} \
		--detach \
		--pod ${POD_NAME} \
		--name ${DATABASE_NAME}-test \
		--env POSTGRES_PASSWORD=${SQL_PASSWORD} \
		--env POSTGRES_USER=${SQL_USERNAME} \
		--env POSTGRES_DB=${SQL_DATABASE}_test \
		--volume ${DATABASE_DATA}-test:${SQL_DATA}:Z \
		${DATABASE_IMAGE}

# database.test.old:
# 	${RUNNER} exec -it ${DATABASE_NAME} \
# 		psql --username ${SQL_USERNAME} --dbname ${SQL_DATABASE} \
# 			--command 'drop database if exists ${SQL_DATABASE}_test'
# 	${RUNNER} exec -it ${DATABASE_NAME} \
# 		psql --username ${SQL_USERNAME} --dbname ${SQL_DATABASE} \
# 			--command 'create database ${SQL_DATABASE}_test'

.PHONY: image.server
image.server:
	${RUNNER} container rm --force ${NAME}-server
	${RUNNER} run ${RUNNER_STATS} \
		--detach \
		--pod ${POD_NAME}\
		--name ${NAME}-server \
		--volume ${PWD}:/var/www/app \
		--volume ./ngnix-default.conf:/etc/nginx/conf.d/default.conf \
		${SERVER_IMAGE}

.PHONY: image.security
image.security:
	${RUNNER} container rm --force ${NAME}-security
	${RUNNER} run ${RUNNER_STATS} \
		--detach \
		--pod ${POD_NAME}\
		--name ${NAME}-security \
		--env-file ./envs/.env.security \
		--volume ${PWD}:/var/www/app \
		${SECURITY_IMAGE} \
		start-dev --import-realm

.PHONY: image.prod
image.prod:
	${RUNNER} container rm -f ${NAME}-prod
	${RUNNER} run ${RUNNER_STATS} \
		--pod ${POD_NAME} \
		--rm -it \
		--name ${NAME}-prod \
		--volume ${PWD}:/app \
		--workdir /app \
		${BACKEND_IMAGE}

.PHONY: image.start
image.start:
	${RUNNER} container rm --force ${NAME}-start
	${RUNNER} run \
		--pod ${POD_NAME} \
		--rm --tty --interactive \
		--name ${NAME}-start \
		--env-file ./envs/.env.db \
		--env-file ./envs/.env.security \
		--volume ${PWD}:/app:Z \
		--workdir /app \
		${BACKEND_IMAGE} \
		bash -c './mvnw clean compile spring-boot:run'

.PHONY: image.repl
image.repl:
	${RUNNER} container rm --force ${NAME}-repl
	${RUNNER} run ${RUNNER_STATS} \
		--pod ${POD_NAME} \
		--rm --interactive --tty \
		--name ${NAME}-repl \
		--volume ${PWD}:/app:Z \
		--workdir /app \
		--env-file ./envs/.env.security \
		--env-file ./envs/.env.db \
		--quiet \
		${BACKEND_IMAGE} \
		bash

.PHONY: image.commands
image.commands:
	${RUNNER} container rm --force ${NAME}-commands
	${RUNNER} run \
		--pod ${POD_NAME} \
		--rm --tty --interactive \
		--name ${NAME}-command \
		--volume ${PWD}:/app:Z \
		--workdir /app \
		--env-file ./envs/.env.db \
		--quiet \
		${BACKEND_IMAGE} \
		bash -c './mvnw --settings ./.mvn/settings.xml $(shell cat ./maven-commands | fzf)'

.PHONY: image.test.integration
image.test.integration:
	${RUNNER} container rm --force ${NAME}-test
	${RUNNER} run \
		--pod ${POD_NAME} \
		--rm --tty --interactive \
		--name ${NAME}-test \
		--volume ${PWD}:/app:Z \
		--workdir /app \
		--env-file ./envs/.env.db \
		--env-file ./envs/.env.security \
		--env SPRING_PROFILES_ACTIVE=test \
		${BACKEND_IMAGE} \
		bash -c './mvnw clean compile test'


.PHONY: image.stats
image.stats:
	${RUNNER} pod stats ${POD_NAME}

.PHONY: image.logs
image.logs:
	${RUNNER} logs ${POD_NAME}

.PHONY: image.build
image.build:
	${RUNNER} build \
		--file ./Dockerfile \
		--tag ${BACKEND_IMAGE}

.PHONY: image.publish
image.publish:
	${RUNNER} push ${BACKEND_IMAGE}


# ============================================= LOCAL

.PHONY: local.api
local.api:
	koto

.PHONY: local.format
local.format:
	google-java-format -r ./src/main/java/dev/easbarba/ruokin/*/***
	google-java-format -r ./src/test/java/dev/easbarba/ruokin/*/***

.PHONY: local.system
local.system:
	guix shell --pure --container

.PHONY: local.commands
local.commands:
	./mvnw --settings ./.mvn/settings.xml $(cat ./maven-commands | fzf)
