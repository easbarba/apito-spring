# ruokin is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ruokin is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ruokin. If not, see <https://www.gnu.org/licenses/>.

ARG OPENJDK_IMAGE=eclipse-temurin:21-jdk

FROM $OPENJDK_IMAGE as BUILDER
WORKDIR /deps
COPY ./mvnw ./pom.xml .
COPY ./.mvn .mvn
RUN ./mvnw dependency:go-offline -B
RUN ./mvnw test-compile -B

FROM $OPENJDK_IMAGE as FINAL
MAINTAINER EAS Barbosa<easbarba@outlook.com>
WORKDIR /app
COPY --from=BUILDER /deps/.m2 .
COPY . .
CMD [ "./mvnw", "clean", "test" ]
