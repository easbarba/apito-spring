<!--
 ruokin-spring is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 ruokin-spring is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with ruokin-spring. If not, see <https://www.gnu.org/licenses/>.
 -->

# Ruokin | Spring

Evaluate soccer referees' performance.

[Vue.js](https://gitlab.com/easbarba/ruokin-vuejs) | [Main](https://gitlab.com/easbarba/ruokin)

## Endpoints

| description | path     |
|-------------|----------|
| Welcome     | /api/v1/ |

PS: `localhost:5000`

## Stack

- [Spring Boot { Framework, WebFlux, Security, Data }](https://spring.io/projects/spring-boot)
- [Jakarta EE](https://jakarta.ee)
- [Keycloak](https://www.keycloak.org)
- [Apache Maven](https://maven.apache.org/)
- [R2DBC](https://r2dbc.io/)
- [Flyway](https://flywaydb.org/)
- [MapStruct](https://mapstruct.org/)
- [TestContainers](https://testcontainers.com)
- [Koto](https://gitlab.com/easbarba/koto)
- [PostgreSQL](https://www.postgresql.org)
- [Vuejs](https://vuejs.org)
- [Podman](https://podman.io)
- [NGNIX](https://nginx.org)
- [Gitlab CI](https://gitlab.com)
- [Github Actions](https://github.com/features/actions)
- [Git](https://git-scm.com)
- [Debian](https://www.debian.org)
- [GNU](https://www.gnu.org) { [Guix](https://guix.gnu.org), [Emacs](https://www.gnu.org/software/emacs), [Make](https://www.gnu.org/software/make), [Bash](https://www.gnu.org/software/bash), [Coreutils](https://www.gnu.org/software/coreutils), [Guile](https://www.gnu.org/software/guile), ... }

## [Tasks](Makefile)

To speed up development some `make` targets are provided.

| targets                | description                                           |
|------------------------|-------------------------------------------------------|
| up                     | spin up containers to development (synced folders)    |
| down                   | shutdown spinned containers                           |
| image.exec             | run commands inside  development container            |
| image.build            | build development container image                     |
| image.publish          | push to registry current development  container image |
| image.test.integration | run integration tests                                 |
| image.test.unit        | run unit tests                                        |

PS: Note that `ruokin` relies on `envs/.env*` env files, so be careful to export its content before running its goals.

## Structure

[Source code](ruokin): all source code of the project goes here
[tests](tests): all integration an unit testing source code of the project goes here
[Documentation](docs): All information about API design, openAPI, and related documentation can be found in the `docs`.
[Bin Folder](bin): There are some handy scripts to easily perform daily tasks, check out.

![podman pod](podman_pod.png)

## Bibliography

- Microservice APIs With examples in Python - Jose Haro Peralta - 2023 Manning
- FastAPI Modern Python Web Development - Bill Lubanovic - OReilly 2023
- Fluent Python - Luciano Ramalho - OReilly 2022
- Mastering API Architecture - James Gough - OReilly 2022

## Misc

For more information on development check out the `CONTRIBUTING.md` document.

## LICENSE

[GNU GENERAL PUBLIC LICENSE Version 3](https://www.gnu.org/licenses/gpl-3.0.en.html)
